FROM python:3.9
LABEL maintainer="Johannes Albrecht"

# set working directory
WORKDIR /app

# copy all files
COPY . .
COPY volume/ /app/volume


# install dependencies
RUN apt-get -y update
RUN pip3 install -r requirements.txt

EXPOSE 12346

ENTRYPOINT ["python"]
CMD ["main.py"]