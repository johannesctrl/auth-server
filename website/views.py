from flask import Blueprint, render_template, request, flash, jsonify, url_for
from flask_cors import cross_origin
from flask_login import login_required, current_user
from .models import Note
from . import db
import json
from .models import User
from werkzeug.security import generate_password_hash, check_password_hash
from flask import current_app as app
from flask_mail import Mail, Message

views = Blueprint('views', __name__)


@views.route('/home', methods=['GET', 'POST'])
@login_required
def home():
    if request.method == 'POST':
        note = request.form.get('note')

        if len(note) < 1:
            flash('Note is too short!', category='error')
        else:
            new_note = Note(data=note, user_id=current_user.id)
            db.session.add(new_note)
            db.session.commit()
            flash('Note added!', category='success')

    return render_template("home.html", user=current_user)


@views.route('/delete-note', methods=['POST'])
def delete_note():
    note = json.loads(request.data)
    noteId = note['noteId']
    note = Note.query.get(noteId)
    if note:
        if note.user_id == current_user.id:
            db.session.delete(note)
            db.session.commit()

    return jsonify({})


@views.route('/postStudyId', methods=['GET', 'POST'])
@cross_origin()
def postStudyId():
    print("postStudyId triggered")
    username = request.authorization["username"]
    password = request.authorization["password"]
    print(username, password)

    user = User.query.filter_by(email=username).first()
    if user:
        if check_password_hash(user.password, password):
            response = 'Logged in successfully!'
            return user.email_sha
        else:
            return "false"
    else:
        return "false"


@views.route('/test-mail', methods=['GET', 'POST'])
@cross_origin()
def test_mail():
    mail = Mail(app)
    msg = Message("Twilio SendGrid Test Email", recipients=["johannes.albrecht@stud.htwk-leipzig.de"])
    msg.body = "This is a test email!"
    msg.html = "<p>This is a test email!</p>"
    mail.send(msg)
    return "Test-Mail has been sent successfully!"
