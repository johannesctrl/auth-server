from website import create_app
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from werkzeug.wrappers import Response
from website import auth
from website import views

app = create_app()
"""
app.register_blueprint(auth.auth, url_prefix='/auth-server')
app.register_blueprint(views.views, url_prefix='/auth-server')
app = create_app()
app.wsgi_app = DispatcherMiddleware(  # add prefix to root url
    Response('Not Found', status=404),
    {'/auth-server': app.wsgi_app}
)
"""


@app.route('/hello-world', methods=["GET"])
def hello_world():
    return "Hello World!"


if __name__ == '__main__':
    # app.run(debug=True, port=12346)
    app.run(debug=False, host="0.0.0.0", port=12346)
