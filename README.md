# auth-server
![](image_1.png)

This project is an authentication server where you can sign up and login. It creates an id by hashing the used mail address. This id can be accessed after logging in.

The server is meant to be used for students to create and check for their identity before/while using an application which generates data of the behavior of a student.

## REST API calls
The service provides the following api calls.

|request method|url suffix|description|
|---|---|---|
|POST|/auth-server/postStudyId|This method accepts a POST-Payload with username and password which are base64 encoded, checks if user exists and returns the mail as a sha value.|